package com;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

public class App {

    public static void main(String[] args) throws FileNotFoundException {

        Scanner in = new Scanner(new File("src/main/resources/data.txt"));
        while (in.hasNext()){
            String[] data = in.nextLine().split(" ");
            if (Integer.parseInt(data[0]) != 0 && Integer.parseInt(data[1]) !=0) {
                Struct struct = new Struct();
                System.out.println(struct.getInfo(Integer.parseInt(data[0]), Integer.parseInt(data[1])));
            }
        }

    }
}
