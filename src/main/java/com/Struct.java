package com;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Struct {

    //n - количество сгибов
    //получаем массив
    public List<Integer> getList(int n){
        if (n == 1)
            return Arrays.asList(1);
        else if (n == 2)
            return Arrays.asList(1, 1, 0);
        else {
            List<Integer> prev_list = getList(n - 1);
            List<Integer> new_list = new ArrayList<Integer>();
            for (int i = 0; i < prev_list.size() * 2 + 1; i++) {
                new_list.add(-1);
            }

            //main job
            for (int i = 0; i < prev_list.size(); i++) {
                new_list.set((i*2 + 1), prev_list.get(i));
            }

            int par = 1;
            for (int i = 0; i < new_list.size()/2 + 1; i++) {
                new_list.set((i*2),par);
                if (par == 1)
                    par = 0;
                else if (par == 0)
                    par = 1;
            }
            return new_list;
        }
    }

    public int getInfo(int n, int m){
        m--; //т.к. в файле отчет начинается с 1, а индексы с 0
        try {
            return this.getList(n).get(m);
        } catch (Throwable e){
            return -1; // изгиба под таким номером нет
        }
    }
}
